# erosnow_test

It includes selenium scripts to test openweather website.

### Prerequisites

Please make sure to have:
1. Java, eclipse, Selenium web-driver installed.
2. Chrome driver, TestNG jar files and other necessary jar files to run Selenium scripts.

## Running the tests

Once user setup everything in eclipse, then (s)he can run tests using "testng.xml". Also, user can modify "test suite" in testng.xml file i.e. run particular (include/exclude tags) methods. 

### Break down into end to end tests

I have added methods for HomePage (verifySocialLink, verifyWidgets, verifyWrongDataForm), for searching city (searchInvalidCity, searchValidCity) and for showing Interactive maps (showMaps - to verify if cities-data shown on interactive map).

User can create 4 test cases for above methods.
1. Test One : can include 3 methods for homepage verification.
2. Test Two : can include searchInvalidCity method.
3. Test Three : can include searchValidCity method.
4. Test Four : can include showMaps method.

## Built With

* eclipse - IDE
* Selenium web-driver
* TestNG - framework for Selenium